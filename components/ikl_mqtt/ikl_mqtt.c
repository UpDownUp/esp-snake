#include <esp_log.h>

#include "ikl_mqtt.h"
#include "snek_buttons.h"

esp_mqtt_client_handle_t mqtt_handle;
conn_state_t conn_state = DISCONNECTED;

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    int msg_id = 0;

    switch (event_id) {
        case (MQTT_EVENT_CONNECTED): {
            printf("MQTT: Connected to MQTT server. Attempting subscribe...\n");
            conn_state = CONNECTED_MQTT;

            msg_id = esp_mqtt_client_subscribe(mqtt_handle, "/espsnek/butts", 0);
            if (msg_id == -1) {
                printf("MQTT: Subscribe failed.\n");
            } else {
                printf("MQTT: sent subscribe successful, msg_id = %d\n", msg_id);
                conn_state = SUBSCRIBED_MQTT;
            }
            break;
        }
        case (MQTT_EVENT_DISCONNECTED): {
            printf("MQTT: MQTT Disconnected. Attempting to reconnect in 20 s.\n");
            break;
        }
        case (MQTT_EVENT_DATA): {
            esp_mqtt_event_handle_t event = event_data;
            printf("MQTT: Data received: msg_id: %d // %s\n", event->msg_id, event->data);

            // a one-character string is expected as button input.
            if (event->data_len == 1) {
                switch (event->data[0]) {
                    case 'U': button_pressed = 23; break;
                    case 'D': button_pressed = 5; break;
                    case 'L': button_pressed = 22; break;
                    case 'R': button_pressed = 18; break;
                }
            }

            break;
        }
        default: {
            printf("MQTT: Event dispatched from event loop base=%s, event_id=%d\n", base, (int) event_id);
        }
    }
}

void mqtt_init(void) {
    esp_mqtt_client_config_t mqtt_init_conf = {
        .broker.address.uri = "mqtt://test.mosquitto.org"
    };
    mqtt_handle = esp_mqtt_client_init(&mqtt_init_conf);
    ESP_ERROR_CHECK(esp_mqtt_client_register_event(mqtt_handle,
                                                   ESP_EVENT_ANY_ID,
                                                   mqtt_event_handler,
                                                   mqtt_handle));
    ESP_ERROR_CHECK(esp_mqtt_client_start(mqtt_handle));
    printf("MQTT init complete.\n");
}

void mqtt_send(const char* data) {
    if (conn_state >= CONNECTED_MQTT){
        esp_mqtt_client_enqueue(mqtt_handle, "/espsnek/dots",
                                data, 0, 0, false,
                                true);
    }
}