# Snake on ESP32
This is snake game built with ESP-IDF for the ESP32. It can run as a server for a [web interface]((https://mqtt-snek.glitch.me/)) and/or locally with physical buttons and a MAX7219 dot matrix display, depending on different `#define` values on `main/app_main.c`.

 This project was built with target **ESP32** and tested on a WROVER-E devkit.

 This repository and its complement web app are licensed under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed.en).

# Quickstart
## Simulated
A [simulation](https://wokwi.com/projects/377671853629288449) on Wokwi contains a version of this repository with all source code contained in one main.c, to bypass Wokwi's limitations on external components, and without `ikl_wifi` and `ikl_mqtt`.

Button input and MAX7219 output is enabled on the simulator, MQTT is not.

`simulation.ino` contains a snapshot of the code running on Wokwi.

## Real
Clone this repository and `cd` into it:

    git clone https://gitlab.com/UpDownUp/esp-snake.git
    cd esp-snake

A little config is necessary first.

### Wifi
By default, only MQTT communication is enabled, but wifi credentials are undefined. 
**Compilation will fail** if wifi credentials are not known to the compiler. You can either manually modify `components/ikl_wifi/ikl_wifi.c` to add them, or run

    echo "add_compile_definitions(WIFI_SSID=\"yourssid\" WIFI_PASS=\"yourpass\" WIFI_AUTH=WIFI_AUTH_WPA2_PSK)" >> components/ikl_wifi/CMakeLists.txt

Replace `WIFI_AUTH_WPA2_PSK` with the correct value for your network from the [wifi_auth_mode_t](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_wifi.html#_CPPv416wifi_auth_mode_t) enum.

### Wifiless
If you really want to compile without wifi, you can also just change `#define MQTT true` in `main/app_main.c` to `#define MQTT false`.

### Build

Once that is sorted out, **if you are building for a target other than ESP32** (eg ESP32-S2), set the correct target:

    idf.py set-target <target> # run idf.py --list-targets if unsure

When target is set, build and flash (and monitor, if you want) normally with `idf.py`.

### Pinout
![edited simulator screencap](pinout.png)

## Frontend
If `MQTT` is defined true, then you can interact with the game on a [web app](https://mqtt-snek.glitch.me/). More information on the game's communication protocol can be found there.

**Playing the game with a physical MAX7219 and buttons has not been tested with real hardware**, but ought to work. It may need further initialization than what worked in the simulator.

### Limitations
MQTT handling code is unoptimized for latency, so playing on the web app may be jittery. If you want to enjoy snake without buttons or a MAX7219, play on the simulator.

Also, no handling is in place for multiple servers/clients! If you find the a game already running on the webapp and you want to test the server yourself, please ask me to close it.

# Options
MAX7219 output and button input are disabled by default in this repository. To enable them, `#define` `BUTTS` and `DOTMATRIX` true in `main/app_main.c`.

All three options `MQTT`, `BUTTS`, and `DOTMATRIX` can be enabled or disabled independently of each other.