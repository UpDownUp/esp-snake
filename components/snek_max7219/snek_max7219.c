#include <driver/spi_master.h>
#include <driver/gpio.h>
#include "snek_max7219.h"

spi_device_handle_t spi_handle;

void max7219_init(void) {
    /* In real life, there might be some additional steps
        needed for init, like setting the intensity register.
        Datasheet wasn't entirely clear about whether it's
        visible at lowest intensity. 
        In the simulator it just werks
    */
    // SPI bus configuration
    spi_bus_config_t bus_config = {
        .mosi_io_num = GPIO_NUM_26,
        .sclk_io_num = GPIO_NUM_14,
    };

    // Initialize the SPI bus
    spi_bus_initialize(VSPI_HOST, &bus_config, 0);

    // SPI device configuration
    spi_device_interface_config_t dev_config = {
        .mode = 0,                    // MAX7219 has CPOL and CPHA == LOW
        .clock_speed_hz = 10000000,   // MAX7219 allows up to 10 MHz
        .spics_io_num = GPIO_NUM_27,
        .queue_size = 1
    };

    // Add the SPI device to the bus
    spi_bus_add_device(VSPI_HOST, &dev_config, &spi_handle);
}

// digit must be 1-8. value must be 0 - 0xFF.
// Digit == row.
void write_digit(uint8_t digit, uint8_t value) {

    // MAX7219 can be written to using a 2-byte bitfield,
    // where byte 1 is the OR combination of set dot masks (1 << 0 to 1 << 7),
    // and bits 8 - 11 (incl) are the register address of the row (0x01 to 0x08).
    uint8_t bitfield[2] = {digit, value};

    spi_transaction_t t = {0};
    t.length = 16;
    t.tx_buffer = bitfield;
    spi_device_transmit(spi_handle, &t);
}

void max7219_render(const uint8_t *map) {
    
    for (uint8_t i = 0; i < 8; i++) {
        write_digit(i + 1, map[i]);
    }
      // With a clock speed of 10 MHz,
      // the delay caused by this operation is trivial.
      // If it were not, get_tick_count measurement
      // can be used to adjust the delay.
}