#ifndef IKL_MQTT
#define IKL_MQTT

#include <mqtt_client.h>

extern esp_mqtt_client_handle_t mqtt_handle;

typedef enum {
    DISCONNECTED,
    CONNECTED_WIFI,
    CONNECTED_MQTT,
    SUBSCRIBED_MQTT
} conn_state_t;

extern conn_state_t conn_state;

void mqtt_init(void);
void mqtt_send(const char* data);

#endif // IKL_MQTT