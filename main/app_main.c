#include <stdio.h>
#include <time.h>
#include <driver/spi_master.h>
#include <driver/gpio.h>
#include "freertos/FreeRTOS.h"
#include <freertos/task.h>

#include "snek_buttons.h"

#define TURN_INTERVAL_MS    1000  // time between each move

#define BUTTS       false
#define DOTMATRIX   false
#define MQTT        true

#if MQTT
    #include "ikl_mqtt.h"
    #include "ikl_wifi.h"
#endif

#if DOTMATRIX
    #include "snek_max7219.h"
#endif

//////////////////////////////// --- GAME ENGINE
#define BORDER_SZ 8
/* For ease of conversion into MAX7219 dots,
   grid is defined like this: 
    1 2 . . . 8
    2
    .
    .
    8
*/

typedef enum {
    dir_N = 0,
    dir_E = 1,
    dir_S = 2,
    dir_W = 3
} dir_t;

typedef struct {
    uint8_t x;
    uint8_t y;
} pos_t;

typedef struct {
    pos_t pos[BORDER_SZ * BORDER_SZ];
    uint8_t length;
    dir_t dir;
} snake_t;

void move_snake(snake_t *snake) {
    // Shift each 'pixel' on the snake by -1 position, except pos[0]
    for (uint8_t i = snake->length - 1; i >= 1; i--) {
        snake->pos[i] = snake->pos[i - 1];
    }

    // move snake.pos[0] based on direction
    switch (snake->dir) {
        case dir_N: snake->pos[0].y--; break;
        case dir_E: snake->pos[0].x++; break;
        case dir_S: snake->pos[0].y++; break;
        case dir_W: snake->pos[0].x--; break;
    }
}

// Generate a new fruit somewhere not on the snake body
pos_t move_fruit(snake_t snake) {

    pos_t fruit;
    bool collides;

    do {
        // Generate random position
        fruit.x = (uint8_t) 1 + ( ((double) rand() / RAND_MAX) * (BORDER_SZ - 1));
        fruit.y = (uint8_t) 1 + ( ((double) rand() / RAND_MAX) * (BORDER_SZ - 1));
        printf("generated fruit x %d y %d\n", fruit.x, fruit.y);
        collides = false;

        // Check each snake pixel for collision
        for (uint8_t i = 0; i < snake.length; i++) {
            if (snake.pos[i].x == fruit.x && snake.pos[i].y == fruit.y) {
                collides = true;
            }
        }
    } while (collides == true);
    return fruit;
}

// return true if game is over.
// Game is over if snake.pos[0] is out of bounds,
// Or has collided with other snake dots.
bool check_gameover_conditions(const snake_t snake) {
  
  // Stage 1: Check if snake.pos[0] is within bounds
  if (snake.pos[0].x <= BORDER_SZ && snake.pos[0].x > 0 
  &&  snake.pos[0].y <= BORDER_SZ && snake.pos[0].y > 0) {
  // If within bounds,
      for (uint8_t i = 1; i < snake.length; i++) {
  // check whether snake.pos[0] collision with other dots
          if (snake.pos[0].x == snake.pos[i].x
          &&  snake.pos[0].y == snake.pos[i].y) {
              return true;
          }
      }
  // if not within bounds, game over
  } else return true;

  // if all checks passed without returning, good to go
  return false;
}


//////////////////////////////// --- COMPONENT HELPERS
#if MQTT
// Only for MQTT. For MAX7219 display, see compose_map().
// Expects an at least 25-byte char array, empty, as input.
void compose_gamestate(char* map, const snake_t snake,
                 const pos_t fruit, const uint8_t turn) {
    
    uint8_t bitmap[12] = {0};
    
// Bytes 0 - 7: Map grid
    for (uint8_t i = 0; i < snake.length; i++) {
        bitmap[snake.pos[i].y - 1] |= 1 << (snake.pos[i].x - 1);
    }

// bitmap[8] contains the position of the fruit,
// encoded as 4 bits for x pos and 4 bits for y pos,
// in 1-8 range.
// Client side will interpret bitmap == 0 as no fruit.
    if (fruit.x && fruit.y) {
        bitmap[8] = ((fruit.x) << 4) + (fruit.y);
    }

// bitmap[9] and [10] contain the
// LSB and MSB, respectively.
    bitmap[9]  =  turn       & 0xFF;
    bitmap[10] = (turn >> 8) & 0xFF;
    bitmap[11] = snake.length;

    sprintf(map, "%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X",
        bitmap[0], bitmap[1], bitmap[2],  bitmap[3], 
        bitmap[4], bitmap[5], bitmap[6],  bitmap[7],
        bitmap[8], bitmap[9], bitmap[10], bitmap[11]);
}
#endif

#if DOTMATRIX
// Only for MAX7219. For MQTT, see compose_gamestate().
// Writes 8-byte grid into uint8_t *map. Expects empty 8-byte map.
void compose_map(uint8_t *map, const snake_t snake, const pos_t fruit) {
    
    // The fruit blinks, for ease of identification.
    static bool show_fruit = true;

    // first, add the fruit to where it should be,
    // or hide it if blinking:
    if (show_fruit) {
        map[fruit.y - 1] |= 1 << (fruit.x - 1);
        // "rows[dot.ypos] |= 1 << (dot.xpos - 1)" syntax
        // is because each row is a 0x00-0xFF bitfield,
        // each bit (0 to 7) representing a segment (dot).
    }
    show_fruit = !show_fruit;

    // then, add each dot of the snake
    for (uint8_t i = 0; i < snake.length; i++) {
      map[snake.pos[i].y - 1] |= 1 << (snake.pos[i].x - 1);
    }
}
#endif

void game_over(void) {
    printf("it's over\n");
    const uint8_t sadface[] = {
        // thanks to xantorohara's LED Matrix editor
        0x00, 0xe7, 0x00, 0x22, 0x00, 0x3c, 0x42, 0x81
        // as before, sadface[0] is unused, using 1-8
    };

    #if MQTT 
        char sadface_mqtt[17] = {0};
        sprintf(sadface_mqtt, "%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X",
        sadface[0], sadface[1], sadface[2], sadface[3],
        sadface[4], sadface[5], sadface[6], sadface[7]);
        // Send 00 as byte 8 to tell the client not to draw a fruit
        mqtt_send(sadface_mqtt);
    #endif

    #if DOTMATRIX
        max7219_render(sadface);
    #endif
}


//////////////////////////////// --- MAIN 
void app_main() {
    
    #if MQTT
        ikl_wifi_init();
        printf("Connecting...\n");
        while (conn_state != SUBSCRIBED_MQTT) {
          vTaskDelay((100) / portTICK_PERIOD_MS);
        }
        printf("Online!\n");
    #endif

    #if DOTMATRIX
        // Display setup
        max7219_init();
    #endif

    #if BUTTS
        // Controls setup
        button_config();
    #endif

    // Setup randomization for fruit positioning
    srand(time(NULL));
    
    while(true) {
        // Game setup
        uint16_t turn = 0;
        snake_t snake = {{{3, 7}, {3, 8}}, 2, dir_N};
        pos_t fruit = {3, 4}; // first fruit is easy

        /* HOW IT WORKS
            At the beginning of each turn, game state is rendered.
            The player is given an interval of time to react.
              This interval is also used to blink the fruit.
            At the end of the interval,
            user input is processed and the game state is updated,
            and the next turn starts.
        */
        
        // Game loop
        while (check_gameover_conditions(snake) == false) {
            // 1. Display game state
            printf("t = %d\t dir = %d   0.x = %d   0.y = %d    f.x = %d   f.y = %d\n"
                         "\t l   = %d   1.x = %d   1.y = %d   \n"
                                "\t\t   2.x = %d   2.y = %d   \n\n",
                    turn, snake.dir, snake.pos[0].x, snake.pos[0].y, fruit.x, fruit.y,
                       snake.length, snake.pos[1].x, snake.pos[1].y,
                                     snake.pos[2].x, snake.pos[2].y);
            
                // Dot matrix display blinks the fruit to distinguish it.
                const uint8_t blinks = 2;
                for (uint8_t i = 0; i < blinks; i++) {
                    #if DOTMATRIX
                        uint8_t map[8] = {0};
                        compose_map(map, snake, fruit);
                        max7219_render(map);
                    #endif
                    vTaskDelay((TURN_INTERVAL_MS / blinks) / portTICK_PERIOD_MS);
                }

            #if MQTT            
                // frontend can display fruit with a different color,
                // as long as it knows where it is.
                char gamestate[25] = {0};
                compose_gamestate(gamestate, snake, fruit, (uint8_t) turn);
                mqtt_send(gamestate);
            #endif

            #if (MQTT || BUTTS)
                // 2. Digest latest input
                if (button_pressed) {
                    switch (button_pressed) {
                      // Do not allow turning 180 degrees
                      case (BTN_UP): if (snake.dir != dir_S) snake.dir = dir_N; break;
                      case (BTN_R):  if (snake.dir != dir_W) snake.dir = dir_E; break;
                      case (BTN_DN): if (snake.dir != dir_N) snake.dir = dir_S; break;
                      case (BTN_L):  if (snake.dir != dir_E) snake.dir = dir_W; break;
                    }
                    printf("butt %d pressed, dir now %d\n", button_pressed, snake.dir);
                    button_pressed = 0;
                }
            #endif

            // 3. Update game state
            move_snake(&snake);
            // If snake eats fruit, increment length
            if (snake.pos[0].x == fruit.x
            &&  snake.pos[0].y == fruit.y) {
                snake.length++;

                // remove the fruit for 1 turn after eating it
                fruit.x = 0;
                fruit.y = 0;
                // after that, put it somewhere random
            } else if (fruit.x == 0 && fruit.y == 0) {
                fruit = move_fruit(snake);
            }

            turn++;
        }

      // show game over sadface for 3 s, then restart
        game_over();
        vTaskDelay(3000 / portTICK_PERIOD_MS);
    }

    // Unreachable, here for perfectionism's sake
    // spi_bus_remove_device(spi_handle);
}