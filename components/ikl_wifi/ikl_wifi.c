// -- Includes
#include <esp_wifi.h>
#include <esp_log.h>
#include <esp_event.h>
#include <nvs_flash.h>

#include "ikl_wifi.h"
#include "ikl_mqtt.h"

// -- Defines
/*
#define WIFI_SSID ""
#define WIFI_PASS ""
#define WIFI_AUTH WIFI_AUTH_WPA2_PSK
// see wifi_auth_mode_t for all possible options.
*/
#ifndef WIFI_SSID
    #error "Define your wifi credentials in ikl_wifi.c first!"
#endif

// -- (File scope) global variables
esp_netif_t * netif_container;

// -- Function declarations
// Handles Wifi specific events
void wifi_event_handler(void *arg, esp_event_base_t event_base,
                        int32_t event_id, void *event_data);

// Handles IP events (IP connect/disconnect)
void ip_event_handler(void *arg, esp_event_base_t event_base,
                        int32_t event_id, void *event_data);

// -- Function definitions
// Initialize netif stack + Wifi
void ikl_wifi_init() {

    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
  //static const char* TAG = "wifi_init";

    esp_netif_init();
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    netif_container = esp_netif_create_default_wifi_sta();

    wifi_init_config_t init_conf = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&init_conf));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT,  ESP_EVENT_ANY_ID, &wifi_event_handler,  NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT,    ESP_EVENT_ANY_ID, &ip_event_handler,    NULL));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid       = WIFI_SSID,
            .password   = WIFI_PASS,
            /* Authmode threshold resets to WPA2 as default if password matches WPA2 standards (pasword len => 8).
             * If you want to connect the device to deprecated WEP/WPA networks, Please set the threshold value
             * to WIFI_AUTH_WEP/WIFI_AUTH_WPA_PSK and set the password with length and format matching to
	         * WIFI_AUTH_WEP/WIFI_AUTH_WPA_PSK standards.
             */
            .threshold.authmode = WIFI_AUTH,
            .sae_pwe_h2e = WPA3_SAE_PWE_BOTH,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) ); // default anyway
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start());
    // From here, the event handler will take care of connecting.
}

void wifi_event_handler(void *arg, esp_event_base_t event_base,
                              int32_t event_id, void *event_data) {
    switch (event_id) {
        case (WIFI_EVENT_STA_START): {  
            printf("Wifi: init complete. Connecting to %s...\n", WIFI_SSID);
            esp_wifi_connect();
            break;  
        }
        case (WIFI_EVENT_STA_CONNECTED): {
            printf("Wifi: Connected.\n");
            conn_state = CONNECTED_WIFI;
            break;
        }
        case (WIFI_EVENT_STA_DISCONNECTED): {
            printf("Wifi: Disconnected.\n");
            conn_state = DISCONNECTED;
            // todo auto-reconnect - needs to be in some kinda wifi task
            break;
        }
        default: {
            printf("evt type %d\n", (int) event_id);
        }
    }
}


void ip_event_handler(void *arg, esp_event_base_t event_base,
                        int32_t event_id, void *event_data) {
    switch (event_id) {
        case (IP_EVENT_STA_GOT_IP): {
            printf("IP: Acquired IP from AP. Starting MQTT...\n");
            conn_state = CONNECTED_WIFI;
            mqtt_init();
            break;  
        }
        case (IP_EVENT_STA_LOST_IP): {
            printf("IP: Lost IP.\n");
            conn_state = DISCONNECTED;
            break;
        }
        default: {
            printf("IP: evt type %d\n", (int) event_id);
        }
    }
}